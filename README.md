# PATMA authentication server

## About
This authentication server can be used across all applications to retrieve a valid user.  
This poject uses ``patma-core``.

## Project specific
#### Ports
8100 for dev and test server  
8000 for prod server

#### Special folders/files to create in directory above (see directory structure in patma-core README)
+ logs
+ authserver-dev.env

## Authserver env file
+ MYSQL_ROOT_PASSWORD=secret-pw
+ MYSQL_DATABASE=appdb
+ MYSQL_USER=appuser
+ MYSQL_PASSWORD=weak
+ SECURE_SALT=mysalt
+ SECURE_PEPPER=mypepper
+ HIBERNATE_DDL=update # none for server deployment
+ FLYWAY_ENABLED=false # true for server deployment
+ STANDARD_ADMIN_NAME=user1
+ STANDARD_ADMIN_PASSWORD=password1
+ INSTALL_DEPENDENCY=true # change to false when setted up $HOME env described in core README

## Using as a skeleton app
+ Copy all files to a new git repo.
+ Change ``artifactId``, ``name`` and ``description`` in ``pom.xml``
+ Set version of ``de.tickets75.patma.core`` in ``pom.xml``, ``bitbucket-pipelines.yml`` and ``docker-compose.*.yml``
+ Take a look if there are any updates of the dependencies.  
https://mvnrepository.com/ for dependencies declared in ``dependencies``  
https://spring.io/projects/spring-boot#learn for spring boot parent
+ Remove entities, repositories, controllers and common stuff you don't need in your new project
+ Adjust your ``EntityFactory`` and ``SecurityConfig`` in ``config``
+ Rename package ``de.tickets75.patma.productmanagement`` to ``de.tickets75.patma.MY_NEW_PROJECT``
+ Adjust your README.md
+ ``Code``
+ Write tests
+ Run the code on your local machine with ``docker-compose up -d``
+ Run tests with ``docker-compose -f docker-compose.mvntest.yml``

#### Run your app on server (test and prod, deployed by bitbucket)
+ Enable pipelines in project settings in bitbucket
+ Add repository variables if needed (at least GIT_USER, GIT_PASSWORD, SSH_HOST, SSH_PORT is needed). Ask Bernd or Jonas to setup the project if you need help/credentials
+ Generate ssh key in bitbucket and add a known host
+ Add the public key to ```~/.ssh/authorized_keys``` on the server
+ Create your basic directory structure also on the server. It is needed in ``test`` and ``prod`` directories. Your env files should have safe values
