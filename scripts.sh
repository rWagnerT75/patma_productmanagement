#!/bin/sh

up() {
    if [ $INSTALL_DEPENDENCY = "true" ]
    then
        mkdir ../dependency
        git -C /usr/src/dependency init
        git -C /usr/src/dependency pull -t https://${GIT_USER}:${GIT_PASSWORD}@bitbucket.org/gittickets75/patma-core.git
        git -C /usr/src/dependency checkout tags/v$1
        mvn -f /usr/src/dependency clean install -DskipTests
        rm -r ../dependency
    fi

    mvn spring-boot:run
}

run() {
    if [ $INSTALL_DEPENDENCY = "true" ]
    then
        mkdir ../dependency
        git -C /usr/src/dependency init
        git -C /usr/src/dependency pull -t https://${GIT_USER}:${GIT_PASSWORD}@bitbucket.org/gittickets75/patma-core.git
        git -C /usr/src/dependency checkout tags/v$1
        mvn -f /usr/src/dependency clean install -DskipTests
        rm -r ../dependency
    fi

    mvn clean package
    java -jar target/app.jar
}

test_bitbucket() {
    pwd=$(pwd)
    mkdir ../dependency
    cd ../dependency
    git init
    git pull -t https://${GIT_USER}:${GIT_PASSWORD}@bitbucket.org/gittickets75/patma-core.git
    git checkout tags/v$1
    mvn clean install -DskipTests
    cd $pwd
    rm -r ../dependency

    mvn ktlint:check && mvn test
}

ssh_deploy() {
    ssh ${SSH_HOST} -p ${SSH_PORT} "sh ./deploy.sh ${1} ${2} patma-${2}"
}

$1 $2 $3
