package de.tickets75.patma.productmanagement.applicationservice

import de.tickets75.patma.core.api.afterbuy.GetShopCatalogs
import de.tickets75.patma.productmanagement.entity.ProductsUpdateAB
import de.tickets75.patma.productmanagement.repository.ProductsUpdateABLogRepository
import de.tickets75.patma.productmanagement.repository.ProductsUpdateABRepository
import de.tickets75.patma.core.api.afterbuy.GetShopProducts
import de.tickets75.patma.core.api.afterbuy.filter.GetShopProductsFilter
import de.tickets75.patma.core.api.shop.BundleShopApi
import de.tickets75.patma.core.api.shop.EventShopApi
import de.tickets75.patma.core.api.shop.LanguageShopApi
import de.tickets75.patma.core.config.BeanFactoryCore
import de.tickets75.patma.core.entity.base.Bundle
import de.tickets75.patma.core.entity.base.Event
import de.tickets75.patma.core.entity.base.EventLanguage
import de.tickets75.patma.core.repository.base.BundleRepository
import de.tickets75.patma.core.repository.base.EventRepository
import de.tickets75.patma.core.repository.base.LanguageRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class ManagementService {
    private val logger = LoggerFactory.getLogger(GetShopProducts::class.java)

    @Autowired
    lateinit var getShopProducts: GetShopProducts

    @Autowired
    lateinit var eventShopApi: EventShopApi

    @Autowired
    lateinit var bundeShopApi: BundleShopApi

    @Autowired
    lateinit var languageRepo: LanguageRepository

    @Autowired
    lateinit var bundleRepo: BundleRepository

    @Autowired
    lateinit var eventRepo: EventRepository

    @Autowired
    lateinit var languageShopApi: LanguageShopApi

    @Autowired
    lateinit var getShopCatalogs: GetShopCatalogs

    @Autowired
    lateinit var updateLogRepo: ProductsUpdateABLogRepository

    @Autowired
    lateinit var updateRepo: ProductsUpdateABRepository

    @Autowired
    private lateinit var beantFactory: BeanFactoryCore

    fun updateJpaData( event: Event) {

    }

    fun updateJpaData( bundle: Bundle) {

    }

    fun updateJpaData( eventLanguage: EventLanguage) {

    }

    fun insertJpaData( event: Event) {

    }

    fun insertJpaData( bundle: Bundle) {

    }

    fun insertJpaData( eventLanguage: EventLanguage) {

    }

    fun updateFromAfterBuy() {
        val lastUpdate = updateRepo.findById(1).orElseGet{
            ProductsUpdateAB(LocalDateTime.now())
        }

        val filter = GetShopProductsFilter.DateFilter(lastUpdate.date, null)
        getShopProducts.importEventsAndBundles(GetShopProductsFilter(mutableListOf(filter)), { true }, { true }, true)
        writeUpdateDate(LocalDateTime.now())
    }

    fun updateFromShop() {

    }

    fun importBundles() {
        try {
            bundeShopApi.importBundlesFor(eventRepo.findAll())
            logger.info("imported bundles")
            bundeShopApi.importLabelsFor(bundleRepo.findAll())
            logger.info("imported labels")
        } catch(e: Exception) {
            throw Exception ("${e.message}, ${eventRepo.findAll().size}")
        }
    }

    fun writeUpdateDate(date: LocalDateTime) {
        val dat = updateRepo.findById(1).orElseGet { ProductsUpdateAB(date) }
        dat.date = date
        updateRepo.saveAndFlush(dat)
    }

    fun importFull() {
       importShop()
       importAfterbuy()
    }

    fun importShop() {
        eventShopApi.importShopEvents(false)
        logger.info("imported events")
        eventShopApi.importPropertiesFor(eventRepo.findAll())
        logger.info("imported properties")
        bundeShopApi.importBundlesFor(eventRepo.findAll())
        logger.info("imported bundles")
        bundeShopApi.importLabelsFor(bundleRepo.findAll())
        logger.info("imported labels")

    }

    fun importAfterbuy() {
        var page: Page<Event> = eventRepo.findAll(PageRequest.of(0, 200))
        val idList: MutableSet<Int> = HashSet()
        page.content.forEach {
            try {
                idList.add(it.afterbuyId!!)
            } catch (e: Exception) {
                logger.info("Element without afterbuyId appeared")
            }
        }
        getShopProducts.importEventsAndBundles(GetShopProductsFilter(
            mutableListOf(
                GetShopProductsFilter.ProductId(idList)
            )
        ), { true }, { true })

        while (page.hasNext()) {
            page = eventRepo.findAll(page.nextPageable())
            idList.clear()
            page.content.forEach {
                idList.add(it.afterbuyId!!)
            }
            getShopProducts.importEventsAndBundles(GetShopProductsFilter(
                mutableListOf(
                    GetShopProductsFilter.ProductId(idList)
                )
            ), { true }, { true })
        }
        logger.info("got afterbuy events")

        var page2: Page<Bundle> = bundleRepo.findAll(PageRequest.of(0, 200))
        idList.clear()
        page2.content.forEach {
            idList.add(it.afterbuyId!!)
        }
        getShopProducts.importEventsAndBundles(GetShopProductsFilter(
            mutableListOf(
                GetShopProductsFilter.ProductId(idList)
            )
        ), { true }, { true })

        while (page2.hasNext()) {
            page2 = bundleRepo.findAll(page2.nextPageable())
            idList.clear()
            page2.content.forEach {
                idList.add(it.afterbuyId!!)
            }
            getShopProducts.importEventsAndBundles(GetShopProductsFilter(
                mutableListOf(
                    GetShopProductsFilter.ProductId(idList)
                )
            ), { true }, { true })
        }
        logger.info("got afterbuy bundles")
    }
}