package de.tickets75.patma.productmanagement.common

import de.tickets75.patma.productmanagement.common.ADMIN_CREATED
import de.tickets75.patma.productmanagement.config.EntityFactory
import de.tickets75.patma.productmanagement.repository.AppUserRepository
import de.tickets75.patma.productmanagement.repository.RoleRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class AppEventListener {
    private val logger = LoggerFactory.getLogger(AppEventListener::class.java)

    @Autowired
    private lateinit var entityFactory: EntityFactory

    @Autowired
    private lateinit var appUserRepo: AppUserRepository

    @Autowired
    private lateinit var roleRepo: RoleRepository

    @Value("\${app.rest.admin.name}")
    private lateinit var adminName: String

    @Value("\${app.rest.admin.password}")
    private lateinit var adminPassword: String

    @EventListener(ApplicationReadyEvent::class)
    fun createStandardAdmin() {
        var admin = appUserRepo.findByUsername(adminName).orElse(entityFactory.getInjectedAppUser())
        if (admin.id == 0) {
            logger.info(ADMIN_CREATED)
            admin.username = adminName
            admin.password = adminPassword
        }

        admin.roles.addAll(roleRepo.findAll())
        appUserRepo.saveAndFlush(admin)
    }
}
