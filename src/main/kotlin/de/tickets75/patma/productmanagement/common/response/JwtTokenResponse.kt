package de.tickets75.patma.productmanagement.common.response

import de.tickets75.patma.core.common.CustomResponse

class JwtTokenResponse(token: String) : CustomResponse {
    val token = token
}
