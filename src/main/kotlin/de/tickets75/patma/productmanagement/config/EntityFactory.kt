package de.tickets75.patma.productmanagement.config

import de.tickets75.patma.productmanagement.entity.AppUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class EntityFactory {
    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Value("\${app.secure.salt}")
    private lateinit var salt: String

    @Value("\${app.secure.pepper}")
    private lateinit var pepper: String

    fun getInjectedAppUser(): AppUser {
        return AppUser(passwordEncoder, pepper, salt)
    }
}
