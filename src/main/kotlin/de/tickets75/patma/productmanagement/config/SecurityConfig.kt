package de.tickets75.patma.productmanagement.config

import de.tickets75.patma.core.config.JwtAuthentication
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.savedrequest.NullRequestCache
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig : WebSecurityConfigurerAdapter() {
    @Autowired
    private lateinit var jwtAuth: JwtAuthentication

    protected override fun configure(http: HttpSecurity?) {
     http!!.cors().and().authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/login").permitAll()
            .antMatchers(HttpMethod.POST, "/login").permitAll().anyRequest()
            .authenticated().and().csrf().disable().requestCache().requestCache(NullRequestCache()).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .addFilter(jwtAuth.JwtAuthenticationFilter(authenticationManager()))
        http!!.cors().and().csrf().disable()
    }


    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val source = UrlBasedCorsConfigurationSource()
        val cors = CorsConfiguration().applyPermitDefaultValues()
        cors.addAllowedOrigin("/**")
        cors.addAllowedMethod("GET")
        cors.addAllowedMethod("POST")
        cors.addAllowedMethod("PUT")
        cors.addAllowedMethod("PATCH")
        cors.addAllowedMethod("DELETE")
        source.registerCorsConfiguration("/**", cors)
        return source
    }
}

