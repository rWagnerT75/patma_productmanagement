package de.tickets75.patma.productmanagement.controller

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import de.tickets75.patma.productmanagement.common.BAD_CREDENTIALS
import de.tickets75.patma.productmanagement.common.response.JwtTokenResponse
import de.tickets75.patma.productmanagement.entity.AppUser
import de.tickets75.patma.productmanagement.repository.AppUserRepository
import de.tickets75.patma.core.common.Converter
import de.tickets75.patma.core.common.CustomResponse
import de.tickets75.patma.core.common.response.MessageResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.http.MediaType
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.servlet.http.HttpServletResponse

@RestController
class AuthenticationController {
    @Autowired
    private lateinit var ctx: ApplicationContext

    @Autowired
    private lateinit var appUserRepo: AppUserRepository

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    private lateinit var privateKey: RSAPrivateKey

    @Autowired
    private lateinit var publicKey: RSAPublicKey

    @Value("\${jwt.expiration}")
    private var jwtExpiration: Long = 0

    @Value("\${jwt.audience}")
    private lateinit var jwtAudience: String

    @Value("\${jwt.issuer}")
    private lateinit var jwtIssuer: String

    @Value("\${app.secure.salt}")
    private lateinit var salt: String

    @Value("\${app.secure.pepper}")
    private lateinit var pepper: String

    /**
     * Check if a given "username" in the [payload] exists and the "password" matches.
     */
    @PostMapping(path = ["/login"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun login(@RequestBody payload: Map<String, String>, response: HttpServletResponse): CustomResponse {
        val appUser = appUserRepo.findByUsername(payload.get("username").orEmpty()).orElse(AppUser())

        if (!passwordEncoder.matches(pepper + payload.get("password").orEmpty() + salt, appUser.password)) {
            response.status = HttpServletResponse.SC_FORBIDDEN

            return MessageResponse(BAD_CREDENTIALS)
        }

        val roles = ArrayList<String>()
        appUser.roles.forEach {
            roles.add(it.name)
        }
        val token = JWT.create().withAudience(jwtAudience).withIssuer(jwtIssuer).withExpiresAt(
            Converter.ldtToDate(LocalDateTime.now(ZoneOffset.UTC).plusSeconds(jwtExpiration))
        ).withClaim("publicId", appUser.id).withArrayClaim("roles", roles.toTypedArray())
            .sign(Algorithm.RSA512(publicKey, privateKey))

        return JwtTokenResponse(token)
    }
}
