package de.tickets75.patma.productmanagement.controller

import de.tickets75.patma.productmanagement.applicationservice.ManagementService
import de.tickets75.patma.core.api.shop.EventShopApi
import de.tickets75.patma.core.common.response.MessageResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import de.tickets75.patma.productmanagement.repository.ProductsUpdateABRepository
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import javax.servlet.http.HttpServletResponse

@RestController
class ImportController {
    @Autowired
    private lateinit var eventShopApi: EventShopApi

    @Autowired
    private lateinit var actionManager: ManagementService

    @Autowired
    private lateinit var updateRepo: ProductsUpdateABRepository

    @PostMapping(path = ["/importAfterbuy"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun importAfterbuy(@RequestBody payload: Map<String, String>, response: HttpServletResponse):MessageResponse{
       actionManager.importAfterbuy()
       actionManager.writeUpdateDate(LocalDateTime.now())
       return MessageResponse("Success")
    }

    @PostMapping(path = ["/importShop"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun importShop(@RequestBody payload: Map<String, String>, response: HttpServletResponse):MessageResponse{
       actionManager.importShop()
       return MessageResponse("Success")
    }

    @PostMapping(path = ["/updateAfterbuy"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateAfterbuy(@RequestBody payload: Map<String, String>, response: HttpServletResponse):MessageResponse{
        actionManager.updateFromAfterBuy()
        return MessageResponse("Success")
    }

    @PostMapping(path = ["/fullImport"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun fullImport(@RequestBody payload: Map<String, String>, response: HttpServletResponse):MessageResponse{
        actionManager.importFull()
        return MessageResponse("Success")
    }

    @PostMapping(path = ["/importBundles"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun importBundles(@RequestBody payload: Map<String, String>, response: HttpServletResponse):MessageResponse{
        actionManager.importBundles()
        return MessageResponse("Success")
    }
}
