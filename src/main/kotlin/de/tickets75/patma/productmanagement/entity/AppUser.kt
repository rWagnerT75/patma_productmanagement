package de.tickets75.patma.productmanagement.entity
import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.crypto.password.PasswordEncoder
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

@Entity
class AppUser constructor() {
    constructor(passwordEncoder: PasswordEncoder, pepper: String, salt: String) : this() {
        this.passwordEncoder = passwordEncoder
        this.pepper = pepper
        this.salt = salt
    }

    @Transient
    private lateinit var passwordEncoder: PasswordEncoder

    @Transient
    private lateinit var salt: String

    @Transient
    private lateinit var pepper: String

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0

    @Column(unique = true, length = 64)
    @NotBlank
    var username: String = ""

    @JsonIgnore
    @Column(length = 60)
    @NotBlank
    @Size(max = 60, min = 60)
    var password: String = ""
        set(value) {
            field = passwordEncoder.encode(pepper + value + salt)
        }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "users_roles",
        joinColumns = [JoinColumn(name = "app_user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "role_id", referencedColumnName = "id")]
    )
    var roles: MutableSet<Role> = HashSet()
}
