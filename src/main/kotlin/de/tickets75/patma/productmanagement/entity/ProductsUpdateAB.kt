package de.tickets75.patma.productmanagement.entity

import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
class ProductsUpdateAB {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0

    var date: LocalDateTime?

    constructor(iDate: LocalDateTime?) {
        date = iDate
    }

    constructor () {
        date = null
    }
}
