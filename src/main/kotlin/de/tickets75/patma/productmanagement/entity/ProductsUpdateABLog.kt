package de.tickets75.patma.productmanagement.entity

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Lob

@Entity
class ProductsUpdateABLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0
    var date: LocalDateTime? = null

    @Lob
    @Column(length = 500000)
    var changedAfterBuyProducts: String? = null
}