package de.tickets75.patma.productmanagement.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.ManyToMany
import javax.validation.constraints.NotBlank

@Entity
class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int = 0

    @Column(unique = true, length = 32)
    @NotBlank
    var name: String = ""

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    var appUsers: MutableSet<AppUser> = HashSet()
}
