package de.tickets75.patma.productmanagement.repository

import de.tickets75.patma.productmanagement.entity.AppUser
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.util.Optional

/**
 * Default [RepositoryRestResource] for [AppUser]s.
 */
@RepositoryRestResource
interface AppUserRepository : JpaRepository<AppUser, Int> {
    /**
     * Returns a single [AppUser] by it's [username] if present.
     */
    fun findByUsername(username: String): Optional<AppUser>
}
