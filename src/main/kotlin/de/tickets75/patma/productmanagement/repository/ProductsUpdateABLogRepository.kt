package de.tickets75.patma.productmanagement.repository

import de.tickets75.patma.productmanagement.entity.ProductsUpdateABLog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface ProductsUpdateABLogRepository  : JpaRepository<ProductsUpdateABLog, Int> {
}