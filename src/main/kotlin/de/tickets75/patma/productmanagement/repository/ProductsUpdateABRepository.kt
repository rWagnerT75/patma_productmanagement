package de.tickets75.patma.productmanagement.repository

import de.tickets75.patma.productmanagement.entity.ProductsUpdateAB
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import java.util.*

@RepositoryRestResource
interface ProductsUpdateABRepository : JpaRepository<ProductsUpdateAB, Int> {
}