package de.tickets75.patma.productmanagement.repository

import de.tickets75.patma.productmanagement.entity.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

/**
 * Default [RepositoryRestResource] for [Role]s.
 */
@RepositoryRestResource
interface RoleRepository : JpaRepository<Role, Int>
