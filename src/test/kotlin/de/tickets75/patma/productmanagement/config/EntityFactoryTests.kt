package de.tickets75.patma.productmanagement.config

import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class EntityFactoryTests {
    @Autowired
    private lateinit var entityFactory: EntityFactory

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Value("\${app.secure.salt}")
    private lateinit var salt: String

    @Value("\${app.secure.pepper}")
    private lateinit var pepper: String

    @Test
    fun `should create properly`() {
        Assertions.assertThat(entityFactory).isNotNull().isInstanceOf(EntityFactory::class.java)
    }

    @Test
    fun `should return injected AppUser`() {
        val appUser = entityFactory.getInjectedAppUser()

        appUser.password = "test"

        Assertions.assertThat(passwordEncoder.matches(pepper + "test" + salt, appUser.password)).isEqualTo(true)
        Assertions.assertThat(passwordEncoder.matches("test" + salt, appUser.password)).isEqualTo(false)
        Assertions.assertThat(passwordEncoder.matches(pepper + "test", appUser.password)).isEqualTo(false)
        Assertions.assertThat(passwordEncoder.matches("test", appUser.password)).isEqualTo(false)
        Assertions.assertThat(passwordEncoder.matches(pepper + "not_matching" + salt, appUser.password))
            .isEqualTo(false)
    }
}
