package de.tickets75.patma.productmanagement.controller

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import de.tickets75.patma.productmanagement.common.BAD_CREDENTIALS
import de.tickets75.patma.productmanagement.common.response.JwtTokenResponse
import de.tickets75.patma.productmanagement.entity.Role
import de.tickets75.patma.core.common.response.MessageResponse
import de.tickets75.patma.productmanagement.repository.AppUserRepository
import de.tickets75.patma.productmanagement.repository.RoleRepository
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.time.ZonedDateTime

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AuthenticationControllerTests {
    @Autowired
    private lateinit var controller: AuthenticationController

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Autowired
    private lateinit var privateKey: RSAPrivateKey

    @Autowired
    private lateinit var publicKey: RSAPublicKey

    @Autowired
    private lateinit var roleRepo: RoleRepository

    @Autowired
    private lateinit var appUserRepo: AppUserRepository

    @Value("\${jwt.audience}")
    private lateinit var jwtAudience: String

    @Value("\${jwt.issuer}")
    private lateinit var jwtIssuer: String

    @LocalServerPort
    private var port: Int = 0

    @Test
    fun `should create properly`() {
        Assertions.assertThat(controller).isNotNull().isInstanceOf(AuthenticationController::class.java)
    }

    @Test
    fun `test login endpoint with existing user`() {
        var role = Role()
        role.name = "ROLE_TEST"
        role = roleRepo.saveAndFlush(role)

        val appUser = appUserRepo.findById(1).or {
            Assertions.fail("User should exist.")
        }

        val set = HashSet<Role>()
        set.add(role)
        appUser.get().roles = set
        appUserRepo.saveAndFlush(appUser.get())

        val request = HttpEntity<LoginEntity>(LoginEntity("test", "test"))
        val response =
            restTemplate.postForEntity("http://localhost:" + port + "/login", request, JwtTokenResponse::class.java)
        val token = response.body!!.token
        val jwtVerifier =
            JWT.require(Algorithm.RSA512(publicKey, privateKey)).withAudience(jwtAudience).withIssuer(jwtIssuer)
                .build()
        val jwt = jwtVerifier.verify(token)

        Assertions.assertThat(response.body).isInstanceOf(JwtTokenResponse::class.java)
        Assertions.assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        Assertions.assertThat(token).isNotBlank()
        Assertions.assertThat(response.body!!.getTimestamp()).isInstanceOf(ZonedDateTime::class.java)
        Assertions.assertThat(jwt.getClaim("publicId").asInt()).isEqualTo(1)
        Assertions.assertThat(jwt.getClaim("roles").asArray(String::class.java).size).isEqualTo(1)
    }

    @Test
    fun `test login enpoint with not existing user`() {
        val request = HttpEntity<LoginEntity>(LoginEntity("not", "existing"))
        val response =
            restTemplate.postForEntity("http://localhost:" + port + "/login", request, MessageResponse::class.java)

        Assertions.assertThat(response.body).isInstanceOf(MessageResponse::class.java)
        Assertions.assertThat(response.statusCode).isEqualTo(HttpStatus.FORBIDDEN)
        Assertions.assertThat(response.body!!.message).isEqualTo(BAD_CREDENTIALS)
        Assertions.assertThat(response.body!!.getTimestamp()).isInstanceOf(ZonedDateTime::class.java)
    }

    inner class LoginEntity(val username: String, val password: String)
}
