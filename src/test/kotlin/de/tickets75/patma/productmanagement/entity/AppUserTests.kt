package de.tickets75.patma.productmanagement.entity

import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class AppUserTests {
    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @Value("\${app.secure.salt}")
    private lateinit var salt: String

    @Value("\${app.secure.pepper}")
    private lateinit var pepper: String

    @Test
    fun `test password matching`() {
        val appUser: AppUser = AppUser(passwordEncoder, pepper, salt)
        appUser.password = "test"

        Assertions.assertThat(passwordEncoder.matches(pepper + "test" + salt, appUser.password)).isEqualTo(true)
        Assertions.assertThat(passwordEncoder.matches("test" + salt, appUser.password)).isEqualTo(false)
        Assertions.assertThat(passwordEncoder.matches(pepper + "test", appUser.password)).isEqualTo(false)
        Assertions.assertThat(passwordEncoder.matches("test", appUser.password)).isEqualTo(false)
        Assertions.assertThat(passwordEncoder.matches(pepper + "not_matching" + salt, appUser.password))
            .isEqualTo(false)
    }
}
