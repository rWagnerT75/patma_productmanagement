package de.tickets75.patma.productmanagement.repository

import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class AppUserRepositoryTests {
    @Autowired
    private lateinit var appUserRepo: AppUserRepository

    @Test
    fun `should create properly`() {
        Assertions.assertThat(appUserRepo).isNotNull().isInstanceOf(AppUserRepository::class.java)
    }

    @Test
    fun `test find appUser by username`() {
        Assertions.assertThat(appUserRepo.findByUsername("test").isEmpty).isEqualTo(false)
        Assertions.assertThat(appUserRepo.findByUsername("not_existing").isEmpty).isEqualTo(true)
    }
}
